function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.
   


/*public class Main {

    public static void main(String[] args) {
        String sentence = "The quick brown fox jumps over the lazy dog";

        // Create an array of size 256 ASCII_SIZE
        int count[] = new int[256];
        int length = sentence.length();

        // Initialize count array index
        for (int i = 0; i < length; i++)
            count[sentence.charAt(i)]++;

        // Create an array of given String size
        char chars[] = new char[sentence.length()];
        for (int i = 0; i < length; i++) {
            chars[i] = sentence.charAt(i);
            int find = 0;
            for (int j = 0; j <= i; j++) {

                // If any matches found
                if (sentence.charAt(i) == chars[j])
                    find++;
            }

            if (find == 1) {
               System.out.println("Occurrence of " + sentence.charAt(i) + " is:" + count[sentence.charAt(i)]);

            }
        }
    }
}
}*/

  describe('[1] Count letters in a sentence.', function() {
    const validLetter = 'o';
    const invalidLetter = 'abc';
    const sentence = 'The quick brown fox jumps over the lazy dog';

    if('Invalid letter returns undefined.', function() {
        const result = source.countLetter(invalidLetter, sentence);
        expect(result).toBeUndefined();
    });

    if('Valid letter returns number of occurrence.', function() {
        const result = source.countLetter(validLetter, sentence);
        expect(result).toEqual(4);
    });
});
}
 



 /*  

function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

   x = false; y = false;
    for(i = 0; i < text.length; i++){
        text1 = text.substring(0,i)
        text2 = text.substring(i)
        x = text1.includes(text.charAt(i))
        y = text2.includes(text.charAt(i))
        console.log(x,text1,text2)
    }
    return x&&y
}
isIsogram("Machine");//False
isIsogram("Hello"); //True
    


function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
     const price = 109.4356;
    const discountedPrice = price * 0.8;
    const roundedPrice = discountedPrice.toFixed(2);

    it('Returns undefined for students aged below 13.', function() {
        const result = source.purchase(12, price);
        expect(result).toBeUndefined();
    });

    it('Returns discounted price (rounded off) for students aged 13 to 21.', function() {
        const result = source.purchase(15, price);
        expect(result).toEqual(roundedPrice);
    });

    it('Returns discounted price (rounded off) for senior citizens.', function() {
        const result = source.purchase(72, price);
        expect(result).toEqual(roundedPrice);
    });

    it('Returns price (rounded off) for people aged 22 to 64.', function() {
        const result = source.purchase(34, price);
        expect(result).toEqual(price.toFixed(2));
    });
};


function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
 const items = [
        { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
        { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
        { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
        { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
        { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
    ];

    it('Returns item categories without stocks.', function() {
        const result = source.findHotCategories(items);
        expect(result).toEqual(['toiletries', 'gadgets']);
    });
};


function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
      const candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'];
    const candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l'];

    it('Returns the array of flying voters.', function() {
        const result = source.findFlyingVoters(candidateA, candidateB);
        expect(result).toEqual(['LIWf1l', 'V2hjZH']);
    });
};


module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};*/